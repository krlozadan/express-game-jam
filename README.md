#Emerging Tech Express Game Jam
###Team Members
- Kinjal
- Rodrigo
- Harish
- Carlos

###Description
Infinite flying game where the player navigates destroying obstacles to score points and avoiding crashing.

###Controls
- Use WASD or Key Arrows to move your ship
- Press space bar to shoot forward
- Use the microphone to make the obstacle explode by saying what's on top of it.

###Key Features
- Procedurally generated map that rerenders with the player movement
- Obstacles are generated using a url request to a Star Wars API. Then serializes the response content to create the obstacles's name and value
- Uses the microphone to detect voice commands and match the names on top of the obstacles (planets) to destroy them.