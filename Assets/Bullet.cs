﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public int bulletSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
				Vector3 newDir = new Vector3(0,0,bulletSpeed*Time.deltaTime);
		transform.Translate(transform.TransformVector(newDir));
	}

}
