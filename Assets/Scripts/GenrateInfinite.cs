﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class Tile{	//CALSS THAT KEEPS TRACK OF GO in the world to be deleted later on

	public GameObject theTile;
	public float creationTime; //Delay to create 

	public Tile(GameObject obj, float _creationTime)
	{
		theTile = obj;
		creationTime = _creationTime;
	}
}

public class GenrateInfinite : MonoBehaviour {

	public GameObject plane; //Smartplane prefab
	public GameObject player; //Player reference to know if we should draw more terrain


	int planeSize = 10; //Size of the terrain to be created
	public int halfTilesX = 5; //How many tiles are around the player
	public int halfTilesZ = 5;

	Vector3 startPos;//Keep track of where player is and where he was
	//We dont want to create new terrain continually, so we want to 
	//CreateAssetMenuAttribute it when the player moved some distance

	Hashtable tiles = new Hashtable();//Hold all GO, indexes.Their name is gonna be created bytheir position

	// Use this for initialization
	void Awake () {
		this.gameObject.transform.position = Vector3.zero;
		startPos = Vector3.zero;

		float updateTime = Time.realtimeSinceStartup;

		for (int x = -halfTilesX; x < halfTilesX; x++) //Minus because we need to keep track of both sides of player
		{
			for (int z = -halfTilesZ; z < halfTilesZ; z++)
			{
				Vector3 pos = new Vector3((x * planeSize + startPos.x),
								0,
								(z * planeSize+startPos.z));

				GameObject _plane = Instantiate(plane, pos, Quaternion.identity);

				string tileName = "Terrain_" + (pos.x.ToString()) + "_" + (pos.z.ToString());
				_plane.name = tileName;
				Tile tile = new Tile(_plane, updateTime);
				tiles.Add(tileName, tile);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		//Check player distance
		int xMove = (int)(player.transform.position.x - startPos.x); //If player has moved more than I plane Size in X
		int zMove = (int)(player.transform.position.z - startPos.z); //If player has moved more than I plane Size in Z

		//If we moved on the X or Z by planSize then update tiles around us
		if(Mathf.Abs(xMove) >= planeSize || Mathf.Abs(zMove) >= planeSize)
		{

			//Time to create tiles
			float updateTime = Time.realtimeSinceStartup; //Timestamp to remove tiles from world
			//If the tile is in between the range of the playe then update his timestamp.
			
			int playerX = (int)(Mathf.Floor(player.transform.position.x/planeSize)*planeSize);
			int playerZ = (int)(Mathf.Floor(player.transform.position.z/planeSize)*planeSize);

			
			for (int x = -halfTilesX; x < halfTilesX; x++) //Minus because we need to keep track of both sides of player
			{
				for (int z = -halfTilesZ; z < halfTilesZ; z++)
				{
					Vector3 pos = new Vector3((x * planeSize + playerX),
									0,
									(z * planeSize+playerZ));
					
					string tileName = "Terrain_" + (pos.x.ToString()) + "_" + (pos.z.ToString());

					if(!tiles.ContainsKey(tileName))
					{
						//Create new tile if the tilename is not inside our hashtable
						GameObject _plane = Instantiate(plane, pos, Quaternion.identity);

						_plane.name = tileName;
						Tile tile = new Tile(_plane, updateTime);
						tiles.Add(tileName, tile);

					}
					else{
						(tiles[tileName] as Tile).creationTime = updateTime; //Update their timeSatamp
					}
				}
			}

			//Destroy all tiles not just created or with time updated
			//and add new titles

			Hashtable newTerrain = new Hashtable();
			foreach (Tile tile in tiles.Values)
			{
				if(tile.creationTime != updateTime)//If the tile is an old one
				{
					//Delete gameobject
					Destroy(tile.theTile);
				}
				else{
					newTerrain.Add(tile.theTile.name, tile); //If we wanna keep it
				}
			}
			//Copy hashtable to our global hashtable
			tiles = newTerrain;

			startPos = player.transform.position;

		}	
		//Draw terrain
	}
}
