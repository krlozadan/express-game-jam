﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APIManager : MonoBehaviour {

	public static APIManager Instance = null;
	private string urlEndpoint;
	private int currentPage;
	private int pageLimit;
	private WWW request;
	private IEnumerator currentState;
	private Response response;
	private EnemySpawner spawner;

	#region Singleton Object

	private void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
			pageLimit = 6;
			currentPage = 1;
			urlEndpoint = "https://swapi.co/api/planets/?page=";
			spawner = FindObjectOfType<EnemySpawner>();
			SetState(StateFetchAPI());
		}
		else if(Instance != this)
		{
			Destroy(gameObject);
			return;
		}
	}

	#endregion

	#region State Machine

	private void SetState(IEnumerator state)
	{
		if(currentState != null)
		{
			StopCoroutine(currentState);
		}
		currentState = state;
		StartCoroutine(currentState);
	}

	IEnumerator StateFetchAPI()
	{
		while(true)
		{
			yield return new WaitForSeconds(3);
			StartCoroutine(MakeRequest());
		}
	}

	IEnumerator MakeRequest()
	{
		request = new WWW(urlEndpoint + currentPage);
		currentPage++;
		yield return request;
		response = JsonUtility.FromJson<Response>(request.text);
		// Tell the Enemy manager to create enemies
		spawner.SpawnEnemies(response.results);
		if(currentPage > pageLimit)
		{
			currentPage = 1;
		}
	}

	#endregion
}
