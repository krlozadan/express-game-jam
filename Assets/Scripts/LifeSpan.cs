﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeSpan : MonoBehaviour 
{
	[SerializeField]
	private float lifeSpan = 5.0f;
	// Use this for initialization
	void Start () 
	{
		Destroy(gameObject, lifeSpan);
	}
	
}
