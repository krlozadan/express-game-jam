﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{

	[SerializeField]
	private Text scoreText;
	static public int Score = 0;
	// Use this for initialization
	void Awake () 
	{
		Enemy.Enemies = new List<Enemy>();
	}

	void Update()
	{
		scoreText.text = "Score : " + Score;
	}

}
