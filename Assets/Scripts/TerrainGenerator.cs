﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Terrain))]
public class TerrainGenerator : MonoBehaviour
{
    #region Globals

    [SerializeField]
    private float _Scale = 10f;

    [SerializeField]
    public float _OffsetX = 0f;

    [SerializeField]
    public float _OffsetY = 0f;

    [SerializeField]
    private float _TextureOrganicBlend = 0.02f;



    [SerializeField]
    private TerrainHeightModifier[] _SplatHeights;




    private Terrain _Terrain;
    private TerrainData _TerrainData;
    private int _SizeX;
    private int _SizeY;
    private int _SizeZ;

    #endregion Globals

    private void Awake()
    {
        _Terrain = GetComponent<Terrain>();
        _TerrainData = _Terrain.terrainData;
        _SizeX = (int)_TerrainData.size.x;
        _SizeY = (int)_TerrainData.size.y;
        _SizeZ = (int)_TerrainData.size.z;
        //Debug.LogFormat("{0}, {1}, {2}", _SizeX, _SizeY, _SizeZ);

        // _OffsetX = UnityEngine.Random.Range(0f,9999999f);
        // _OffsetY = UnityEngine.Random.Range(0f,9999999f);

    }

    private void Update()
    {
    }

    [ContextMenu("DoGenerate")]
    private void DoGenerate()
    {
        Awake();
        _TerrainData.SetHeights(0, 0, GenerateHeights());
        _TerrainData.SetAlphamaps(0,0,GenerateSplatData());

    }


    private float[,] GenerateHeights()
    {
        float[,] heights = new float[_SizeX, _SizeZ];
        for(int z = 0; z < _SizeZ; ++z)
        {
            for (int x = 0; x < _SizeZ; ++x)
            {
                heights[z, x] = GetPerlinHeightsAt(x, z);
            }
        }

        return heights;
    }

    private float GetPerlinHeightsAt(int x, int y)
    {
        float xCoord = (float)x / _SizeX * _Scale + _OffsetX;
        float yCoord = (float)y / _SizeZ * _Scale + _OffsetY;        
        return Mathf.PerlinNoise(xCoord, yCoord);
    }

    private float[,,] GenerateSplatData()
    {
        int mapSize = _TerrainData.alphamapWidth;
        float[,,] splatMapData = new float[_SizeX,_SizeZ, _SplatHeights.Length];

        for(int z = 0; z < _SizeZ; ++z)
        {
            for (int x = 0; x < _SizeX; ++x)
            {
                float terrainHeight = _TerrainData.GetHeight(z,x);
                float[] splats = new float[_SplatHeights.Length];

                for (int i = 0; i < splats.Length; i++)
                {
                    float PerlinNoise = MapFloat( Mathf.PerlinNoise(x * _TextureOrganicBlend, z*_TextureOrganicBlend), 0f, 1.0f, 0.6f, 1f);
                    float curStartingHeight = _SplatHeights[i].StartingHeight * PerlinNoise - _SplatHeights[i].Overlap * PerlinNoise;
                    float nextStartingHeight = 0;
                    if(i != _SplatHeights.Length -1)
                    {
                        nextStartingHeight = _SplatHeights[i+1].StartingHeight * PerlinNoise + _SplatHeights[i].Overlap * PerlinNoise;
                    }

                    if(i == _SplatHeights.Length - 1 && terrainHeight >= curStartingHeight)
                    {
                        splats[i] = 1f; // Completly paint this terrain if the height is greater that value on _SplatHeights
                    }else if( terrainHeight >= curStartingHeight && terrainHeight <= nextStartingHeight)
                    {
                        splats[i] = 1f;
                    }
                }

                NormalizeVec(splats); //Normalize light at the splats
                for (int i = 0; i < _SplatHeights.Length; i++)
                {
                    splatMapData[x,z,i] = splats[i];
                }
            }
        }

        return splatMapData;
    }

    private float NormalizeVec(float [] v)
    {
        float total = 0;
        for (int i = 0; i < v.Length; i++)
        {
            total += v[i];
        }

        for (int i = 0; i < v.Length; i++)
        {
            v[i] /= total;
        }

        return total;
    }

    private float MapFloat(float val, float min1, float max1, float min2, float max2)
    {
        return (val - min1) * (max2 - min2) / (max1 - min1) + min2;
    }

}

[System.Serializable]
public class TerrainHeightModifier
{
    public int StartingHeight;
    public int Overlap;
}