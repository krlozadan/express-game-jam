﻿using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour 
{
	public static List<Enemy> Enemies;

	[SerializeField]
	private TextMesh nameText;
	[SerializeField]
	private TextMesh valueText;
	[SerializeField]
	private float lifeSpan = 10.0f;
	[SerializeField]
	private float speed = 5.0f;
	[SerializeField]
	private float textVerticalSpace = 2.0f;

	internal string planetName;
	internal int value;

	private void Awake()
	{
		Enemies.Add(this);
	}

	private void OnDestroy()
	{
		Enemies.Remove(this);
	}

	// Update is called once per frame
	private void Update () 
	{
		transform.Translate(Vector3.forward * speed * Time.deltaTime);
	}	

	public void Initalize(Planet planet)
	{		
		planetName = planet.name;
		nameText.text = planetName;

		value = planet.rotation_period;
		valueText.text = value.ToString();

		Vector3 newPos = nameText.transform.position;
		newPos.y = Enemies.Count % 2 == 0 ? nameText.transform.position.y : nameText.transform.position.y - textVerticalSpace;
		nameText.transform.position = newPos;

		Destroy(gameObject, lifeSpan);
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Bullet"))
		{
			Destroy(other.gameObject);
			value--;
			GameManager.Score += 100;
			valueText.text = value.ToString();
			if(value <= 0) {
				Destroy(gameObject);
			}
		}
	}
}
