﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider))]
public class Projectile : MonoBehaviour 
{
	[SerializeField]
	private float speedOfBullet = 100.0f;

	// Use this for initialization
	void Start () 
	{
		GetComponent<Rigidbody>().AddForce(transform.forward * speedOfBullet, ForceMode.Impulse);
	}

	void OnTriggerEnter(Collider other)
	{
		Destroy(gameObject);
	}
	
}
