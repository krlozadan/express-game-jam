﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//https://www.youtube.com/watch?v=dycHQFEz8VI

public class EndlessTerrain : MonoBehaviour { 


	public int heightScale = 15;
	public float detailScale = 5.0f;

	// Use this for initialization
	void Awake () {
		Mesh mesh = GetComponent<MeshFilter>().mesh;
		Vector3[] vertexes = mesh.vertices;

		for (int i = 0; i < vertexes.Length; i++)
		{
			vertexes[i].y = Mathf.PerlinNoise( (vertexes[i].x + this.transform.position.x) / detailScale,
							(vertexes[i].z + this.transform.position.z)/detailScale) * heightScale;
		}
		mesh.vertices = vertexes;
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
		gameObject.AddComponent<MeshCollider>();
	}
	
	// Update is called once per frame
	void Update () {

	}
}
