﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class PerlinNoiseTest : MonoBehaviour {

    #region Seializables

        [SerializeField]
        private int _Width = 256;

        [SerializeField]
        private int _Height = 256;

        [SerializeField]
        private float _Scale = 10f;

        [SerializeField]
        private float _OffsetX = 0f;

        [SerializeField]
        private float _OffsetY = 0f;

    #endregion Serializables

    #region Globals

    private Renderer _Rend;

    #endregion Globals

    #region Lifecycle

        private void Awake()
            {
            _Rend = GetComponent<Renderer>();
            }

    private void Update()
    {
        _Rend.material.mainTexture = GenerateTexture();
    }


    #endregion Lifecycle

    private Texture2D GenerateTexture()
    {
        Texture2D tex = new Texture2D(_Width, _Height);

        for (int y = 0; y < _Height; y++)
        {
            for (int x = 0; x < _Width; x++)
            {
            Color color = GetColor(x, y);
                tex.SetPixel(x, y, color);//Change the color of every pixel to the generated color based on width and height of texture
            }

        }
        tex.Apply();//apply the texture
        return tex;
    }

    private Color GetColor(int x, int y)
    {
        float xCoord = (float)x / _Width * _Scale + _OffsetX;
        float yCoord = (float)y / _Width * _Scale + _OffsetY;
        float Sample = Mathf.PerlinNoise(xCoord, yCoord);
        return new Color(Sample, Sample, Sample);
    }
}
