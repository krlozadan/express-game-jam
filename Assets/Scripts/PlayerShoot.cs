﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour 
{
	[SerializeField]
	Projectile bulletPrefab;
	[SerializeField]
	Transform bulletSpawnPosition;
	[SerializeField]
	float timeToWaitForBeforeFiringAgain = 0.5f;

	private bool canFire = true;


	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKey(KeyCode.Space) && canFire)
		{
			Instantiate(bulletPrefab, bulletSpawnPosition.position, bulletSpawnPosition.rotation);
			canFire = false;
			StartCoroutine(Timer(timeToWaitForBeforeFiringAgain));
		}
	}

	IEnumerator Timer(float time)
	{
		yield return time;
		canFire = true;
	}
}
