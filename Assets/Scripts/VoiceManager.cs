﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class VoiceManager : MonoBehaviour 
{
	private KeywordRecognizer keyWordRecognizer;
	string[] nameOfPlanets = {"Fire", "Alderaan", "Yavin IV", "Hoth", "Dagobah", "Bespin", "Endor", "Naboo", "Coruscant", "Kamino", "Geonosis", "Utapau", "Mustafar", "Kashyyyk", "Polis Massa", "Mygeeto",
								"Felucia", "Cato Neimoidia", "Saleucami", "Stewjon", "Eriadu", "Corellia", "Rodia", "Nal Hutta", "Dantooine", "Bestine IV", "Ord Mantell", "unknown", "Trandosha",
								 "Socorro", "Mon Cala", "Chandrila", "Sullust", "Toydaria", "Malastare", "Dathomir", "Ryloth", "Aleen Minor", "Vulpter", "Troiken", "Tund", "Haruun Kal","Cerea",
								 "Glee Anselm", "Iridonia", "Tholoth", "Iktotch", "Quermia", "Dorin", "Champala", "Mirial", "Serenno", "Concord Dawn", "Zolan", "Ojom", "Skako", "Muunilinst", 
								 "Shili", "Kalee", "Umbara", "Tatooine"};

	void Start()
	{
		// Creating a new recognizer for a new Wave
		keyWordRecognizer = new KeywordRecognizer(nameOfPlanets);
		keyWordRecognizer.OnPhraseRecognized += DestroyOnKeyWordRecognized;
		keyWordRecognizer.Start();
	}

	void DestroyOnKeyWordRecognized(PhraseRecognizedEventArgs args)
	{
		foreach (Enemy enemy in Enemy.Enemies)
		{
			if (enemy.planetName == args.text)
			{
				// Destroying the object when a planet's name is planet
				GameManager.Score += enemy.value * 100; 
				Destroy(enemy.gameObject);
				return;
			}
		}
	}

}
