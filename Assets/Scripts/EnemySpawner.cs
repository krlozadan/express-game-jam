﻿using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	[SerializeField]
	GameObject enemyPrefab;
	
	[SerializeField, Range(1.0f, 10.0f)]
	private float step;

	private float distance;

	private void Awake()
	{
		Reset();
		if(enemyPrefab == null)
		{
			Debug.LogWarning("No planet prefab selected");
		}
	}

	public void SpawnEnemies(Planet[] planets)
	{ 
		foreach(Planet planet in planets)
		{	
			GameObject newEnemy = Instantiate(enemyPrefab, transform.position + (Vector3.right * distance), Quaternion.identity);
			newEnemy.GetComponent<Enemy>().Initalize(planet);
			distance += step;
		}
		Reset();
	}

	private void Reset() 
	{
		distance = 0.0f;
	}
}
