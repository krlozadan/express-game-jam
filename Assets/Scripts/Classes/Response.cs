﻿[System.Serializable]
public class Response
{
	public int count;
	public string next;
	public string previous;
	public Planet[] results;
}