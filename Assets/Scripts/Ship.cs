﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ship : MonoBehaviour 
{
	private float mover;

	[SerializeField]
	private float xMovSpeed;
	[SerializeField]
	private float zMovSpeed;
	[SerializeField]
	private float yMovSpeed;
	[SerializeField]
	private Slider healthBar;
	[SerializeField]
	private float damageTakenOnCollision = 10.0f;

	private float XINPUT;
	private float YINPUT;
	public float shipHealth = 100;

	public GameObject Bullet;

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		//print(Input.GetAxis("Horizontal"));
		XINPUT = Input.GetAxis("Horizontal");
		YINPUT = Input.GetAxis("Vertical");
		ClampPosY();
		Vector3 newDir = new Vector3(-xMovSpeed*XINPUT*Time.deltaTime,
									-yMovSpeed*YINPUT*Time.deltaTime,
									zMovSpeed*Time.deltaTime);
		transform.Translate(transform.TransformVector(newDir));

		zMovSpeed+= 0.01f;

		if (Input.GetKeyDown(KeyCode.Space))
		{
			Instantiate(Bullet, transform.position, transform.rotation);
		}

		// Updating the healthbar
		if (healthBar != null)
		{
			healthBar.value = shipHealth / 100;
		}
	}

	private void ClampPosY()
	{
		if(transform.position.y >= 25)
		{
			if(YINPUT > 0)
			{
				yMovSpeed = 0;
			}
			else
			{
				yMovSpeed = 30;
			}
		}

		if(transform.position.y <=20)
		{
			if(YINPUT < 0)
			{
				yMovSpeed = 0;
			}
			else
			{
				yMovSpeed = 30;
			}
		}


	}

	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Terrain") || other.CompareTag("Planet"))
		{
			shipHealth -= damageTakenOnCollision;
		}
	}

}
