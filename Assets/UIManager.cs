﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {


	[SerializeField]
	private GameObject GameOverPanel;
	[SerializeField]
	private GameObject player;
	private bool loseState = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(player.GetComponent<Ship>().shipHealth <= 0)
		{
			loseState = true;
		}

		CheckGameOver();
	}

    private void CheckGameOver()
    {
        if(loseState)
		{
			GameOverPanel.SetActive(true);
			Time.timeScale = 0;
		}
    }

	public void Retry()
    {
		SceneManager.LoadScene(1);
    }

	public void Exit()
    {
		Application.Quit();
    }
}
