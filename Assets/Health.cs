﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour {


	public int health;
	// Use this for initialization
public void Awake()
{
	health = 1000;
}
	
	// Update is called once per frame
	void Update () {
		if(health <= 0 )
		{
			SceneManager.LoadScene(0);
		}
	}
}
